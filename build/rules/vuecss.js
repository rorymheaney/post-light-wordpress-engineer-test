const config = require('../app.config')
const path = require('path')
const ExtractTextPlugin = require("extract-text-webpack-plugin")

module.exports = {
    include: config.paths.scripts,
    exclude: config.paths.sass,
    test: /\.scss$/,
    use: [
      {loader: 'vue-style-loader'},
      {loader:'css-loader'},
      {loader: 'sass-loader',
        options: {
            sourceMap: true,
            includePaths: [
                path.resolve(__dirname, 'node_modules'),
            ],
        }
      }
    ]
  }
