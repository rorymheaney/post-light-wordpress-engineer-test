<?php
/**
 * Template Name: Home
 * Description: Home Page Template - see pages and partials
 */
// $start = TimberHelper::start_timer();
$context = Timber::get_context();
	// Query is made here when including anything that isn't a global
	// call, lets say included an instgram or facebook feed in the footer,
	// in this case, we are calling these here because its only on the home page
	// for this current example
	
	
	// post type movie reviews
		$movieReviewArgs = array(
			// Get post type project
			'post_type' => 'movie_review',
			// Get all posts
			'posts_per_page' => 3,
		);

		$context['MOVIE_REVIEWS'] = Timber::get_posts( $movieReviewArgs );
	// end post type

	// post type movie quotes
		$moveQuoteArgs = array(
			// Get post type project
			'post_type' => 'movie_quote',
			// Get all posts
			'posts_per_page' => 5,
		);

		$context['MOVIE_QUOTES'] = Timber::get_posts( $moveQuoteArgs );
	// end post type

	// post type movie trailers
		$movieTrailerArgs = array(
		    // Get post type project
		    'post_type' => 'movie_trailer',
		    // Get all posts
		    'posts_per_page' => 5,
		);

		$context['MOVIE_TRAILERS'] = Timber::get_posts( $movieTrailerArgs );
	// end post type


Timber::render('pages/front-page.twig', $context);

// echo TimberHelper::stop_timer( $start);