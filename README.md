# Twig WordPress

## Requirements

| Prerequisite    | How to check | How to install
| --------------- | ------------ | ------------- |
| NVM 8.11.1  | `nvm list`    | [NVM](https://github.com/coreybutler/nvm-windows), `nvm install 8.11.1`, this will handle your NODE and NPM packages for you! |
| PHP >= 7.x.x    | `php -v`     | [php.net](http://php.net/manual/en/install.php), if not using a local environment tool|
| MySql >= 5.6.x  |  ---   | --- |

## Features

* [Timber](https://www.upstatement.com/timber/) a faster, easier and more powerful way to build themes, it basically makes sure you can use all the WordPress magic with TWIG 
* [Twig, templating engine](https://twig.symfony.com/),
* [Timber, adds Twig to WordPress!](https://timber.github.io/docs/)
* [Webpack 4](https://webpack.js.org/), bundling assets
* [Foundation Zurb](https://github.com/zurb/foundation-sites), front-end framework

### Additional Features that are availble if needed!
* [Axios](https://github.com/axios/axios), Promise based HTTP client for the browser and node.js
* [Vue](https://github.com/vuejs/vue),  A progressive, incrementally-adoptable JavaScript framework for building UI on the web.
* [AOS Scroll](https://github.com/michalsnik/aos),  Animate on Scroll

## For if you dont want to use the .git folder
* [Bitbucket Link](https://bitbucket.org/rorymheaney/post-light-wordpress-engineer-test/src/master/)


## Tasks Completed
* Add support for 3 different post types: an article (for movie reviews), a video (for movie trailers), and a quote. Style each distinctly. The design doesn’t matter, but each post type should be recognizable.
    * tmpl-home.php
        * Add each query to content, this passes its object to the page, essentially the while-loop before hand
        * Partials for these sections are in "templates/views/home"
            * these partials contain blocks and elements that arguable could be broken out into Style Guide components if this were an actual website and were resuable or contained resuable features. (this applies to the MOVIE DB feed as well)
    * Admin:
        * Post Types were created using [Custom Post Type UI](https://wordpress.org/plugins/custom-post-type-ui/).  I use this as a default CPT creater, its simple / straight forward and easy to extend. 

    
* Add a required custom field to movie reviews (not videos or quotes) called “Rating” which allows the writer to assign anywhere from 1 to 5 stars to a particular movie. Display the movie’s rating as a number of stars in the post.
    * This was done using [ACF](https://www.advancedcustomfields.com/)
    * ACF was also used for the Movie Trailers YouTube ID field. 

* Get a list of movies that are currently in theaters from The Movie DB API, using the discover endpoint. Display a list of the movie posters on the front page under a “Now in Theaters” header.
    * In /lib/api/movie-db.php, you'll see notes on the following:
        * adding the Array to the global content
        * how its done, and when the Transient is set.  
        * there are also notes as to what is going on in each step and why

* Cache the payload of the API request you set up in the previous task in WordPress and set it to expire every 24 hours.
    * In the previous task "/lib/api/movie-db.php" is mentioned. This is where the Transient is set for 24 hours.  

### Bonus
* ADA documenation as well as to where additional things would go if this were a full blown site. 
* Included a VUE setup I've been working on.  It's part of my regular Webpack set up, but I .vue components included now and figured it should be made clear it's part of my starting set up (this isn't set up for Production Assets yet)

### CSS / JS / TWIG / PHP
* There are a lot fo default files I kept included to give a better understanding as to how I start most my projects, some of them also include tasks that were also optional to complete:
    * Add a custom field to an endpoint "/lib/api/", both 'better-featured-img.php' & 'better-taxonomies.php'.  In my bitbucket snippets I also have code I use for when I extend the REST API for my own endpoints. 
    * not all of these files make the final project, but it creates a good starting point. 

### Plugins (from top to bottom in the admin, and default in most my projects)
* Admin Bar Toggle, not neccesary, just a simple way to hide and show the Toolbar when logged in. 
* ACF Pro
    * custom meta fields
* ACF Image Crop
    * used in almost all my projects, so the client can set their own crop and not rely on WP auto cropping
* All-in-one WP Migration 
    * great tool for migrating / setting up sites across other developers, specifically when tied into a build process like Octopus (the entire core install would be in the respository)
        * there is a CLI that can then also be triggered on commits if there is an export from that plugin included, this could then be ran to update the environemnt and its DB / Images / ETC.
        * you would then kick of the 'npm' commands automatically and build out the new dev and/or productions assets
* Custom Post Types UI
    * creating custom post types.
* Error Log Monitor
    * just a nice way to view errors in the dashboard if needed
* Gravity Forms
    * creating forms!
* Simple Custom Posts Order
    * clients often want to re-arrange an post or 2 and done want to have to set the order on their own, so its a simple drag and drop to handle that for them
* Smush
    * this is used if the client doesn't have a design team or member who manages their assets before uploading.  So we can help prevent some size here and there
* Timber
    * Adds the TWIG templating engine to WordPress
* WP GraphQL
    * using GraphQL with REST and/or instead of
* WP REST Filter
    * Because this should have been kept in the API in the first place!
* WP-Optimize
    * clean up as you develop and or the client creats content and deletes, etc

## Documentation

### Change your proxy url
* build > webpack.config.js > BrowserSyncPlugin > update to use your currently proxy URL for your local install 

### From the command line, when working inside of the theme
* cd into theme directory
    * npm: `npm install`
        * will install all your npm packages
    * watch: `npm run watch`
        * builds dev assets to public directory and watches assets for changes!
        * sets up browsersync
    * dev: `npm run dev`
        * builds dev assets to public directory
    * prod: `npm run prod`
        * builds production assets to public directory

### Assets Folder
* scripts / css / fonts / images will go inside the /assets directory
    * You'll see multple folders inside of /assets containing said files
* JS is used along side Dom - Based routing, 
    * You can see this inside of /assets/scripts/pages.js

## Install the project!
* Install WordPress locally or on the host of your choosing (use PHP 7.0 or greater)
* Install [All-in-One WP Migration](https://wordpress.org/plugins/all-in-one-wp-migration/) from the Admin > Plugins > Add New > type "All-in-One WP Migration" > Activate > Install
    * once installed, go to All-in-One WP Migration > Import
    * select (or drag and drop) the file included with the zipped theme: file-name.wpress
        * it will upload, select proceed (it wil restore the files)
        * then click "reset permalinks"
        * it will prompt you to login again:
            * see "Install-instructions.txt"

* Build Assets are already built out inside of the themes "public" folder
    * to build out your own, please follow the command line instructions above