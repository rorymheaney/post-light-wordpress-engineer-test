// for ie
import 'idempotent-babel-polyfill';

// foundation zurb js
import '../../node_modules/foundation-sites/dist/js/foundation.js';

// owl
import 'owl.carousel';

// axios
window.axios = require('axios');

// pages
import './pages.js'