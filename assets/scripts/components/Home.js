// set youtube video on click
function loadYouTubeVideos(){

    let $youTubeThumbnail = $('[data-js="load-yt-video"]'),
        $videoContainer = $('[data-js="remove-iframe"]');
    // 2. This code loads the IFrame Player API code asynchronously.
    // console.log('test');
    let tag = document.createElement('script');

    tag.src = "https://www.youtube.com/iframe_api";
    let firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    // 3. This function creates an <iframe> (and YouTube player)
    //    after the API code downloads.
    let player;
    function onYouTubeIframeAPIReady(vidID) {
		player = new YT.Player('you-tube-player', {
			height: '390',
			width: '640',
			videoId: vidID,
			playerVars: { 
				'controls': 0, 
				'rel': 0
			},
			events: {
				'onReady': onPlayerReady
			}
		});

	  $videoContainer.append('<div id="you-tube-player"></div>');
	  $videoContainer.find('iframe').focus();
    }


    // 4. The API will call this function when the video player is ready.
    function onPlayerReady(event) {
      event.target.playVideo();
    }
    
    $youTubeThumbnail.on('click',function(){
        // get video id
        let vidId = $(this)[0].dataset.ytid;

        $videoContainer.find('iframe').remove();
        $youTubeThumbnail.removeClass('thumbnails__movie--active');
        $(this).addClass('thumbnails__movie--active');
        
        // console.log(vidId)
        // pass id to iframe api
        onYouTubeIframeAPIReady(vidId);

    });

}

// set the product carousel up
function initQuoteCarousel(){
    let $movieQuoteCarousel = $('[data-js="quotes-carousel"]');

	$movieQuoteCarousel.owlCarousel({
        loop:true,
        margin:0,
        nav:false,
        autoplay:false,
        items: 1
    });

}

module.exports = {
    loadYouTubeVideos,
    initQuoteCarousel
};