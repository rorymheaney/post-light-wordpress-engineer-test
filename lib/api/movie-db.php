<?php

// add our api call to the global context
add_filter( 'timber_context', 'fancySquares_add_movie_db_to_context'  );

// add our api call results into out new object
// this object can be accessed globally
// we could do all of this inside of tmpl-home.php technically and not make it global,
// but that would be sloppy, and we aren't slowing anything down this way. 
function fancySquares_add_movie_db_to_context( $context ) {
    $context['fancySquaresMovieDB'] = fancySquares_get_movie_db();
    return $context;
}

// set up are api call
function fancySquares_get_movie_db()
{
    // is there a transient set up?
    // if so return it
    if(get_transient('movieDBTransient')) {
        return get_transient('movieDBTransient');
    } else {
        // no transiet in place, 
        // so lets create one

        // our api key
        $key = 'b01347520634973428466ea15b50616e';
        // todays date
        $todaysDate = date('Y-m-d');
        // last seven days (1 week)
        $lastSevenDays = date('Y-m-d', strtotime("-1 week"));

        // make the request
        $apiRequest = wp_remote_request("https://api.themoviedb.org/3/discover/movie?api_key=". $key ."&language=en-US&primary_release_date.gte=". $lastSevenDays ."&primary_release_date.lte=".$todaysDate);
        
        $apiResult = json_decode($apiRequest['body']);
        $movies = [];

        // loop through the results and
        // assemble them into what will feed into our object above
        for($i = 0; $i < 20; $i++) {
            $movies[$i] = [];
            $movies[$i]['poster'] = $apiResult->results[$i]->poster_path;
            $movies[$i]['title'] = $apiResult->results[$i]->title;
            $movies[$i]['excerpt'] = $apiResult->results[$i]->overview;
            $movies[$i]['date'] = $apiResult->results[$i]->release_date;
        }

        // we've made a call,
        // store it in a transient for 24hrs to limit request
        set_transient('movieDBTransient', $movies, 24 * HOUR_IN_SECONDS); // expires every day
        return $movies;
    }

}